# Installation
 - Make sure `git` and some sort of [nerd-font](https://github.com/ryanoasis/nerd-fonts) is installed and set as the terminal's profile font
 - Make sure your terminal supports 24 bit color.
  - `$ echo $TERM`
  - Should be something like `xterm-256color`
 - `$ git clone https://gitlab.com/btleffler/dot-vim ~/.config/nvim`
 - `$ cd ~/.config/nvim`
 - `$ git submodule update --init --recursive`
  - `--recursive` probably isn't needed, but whatever
 - Open nvim for the first time
 - There will probably be some sort of error about things not being installed, ignore it. We're about to install everything with...
 - `:NeoBundleInstall`


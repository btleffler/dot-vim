filetype off

set rtp+=~/.config/nvim/bundle/neobundle.vim
call neobundle#begin(expand('~/.config/nvim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'mhartington/oceanic-next'
NeoBundle 'vim-airline/vim-airline'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'jistr/vim-nerdtree-tabs'
NeoBundle 'airblade/vim-gitgutter'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'ryanoasis/vim-devicons'

call neobundle#end()

filetype plugin indent on

set colorcolumn=80
set tabstop=2
set backspace=indent,eol,start
set ruler
set number
set showcmd
set incsearch
set hlsearch
set mouse=a
set updatetime=250

syntax enable
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
colorscheme OceanicNext
set background=dark

hi clear SignColumn

set laststatus=2
let g:airline#extensions#hunks#non_zero_only = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_detect_paste=1
let g:airline_theme='oceanicnext'

let NERDTreeShowHidden=1
map <C-\> :NERDTreeToggle<CR>

let g:syntastic_aggregate_errors = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

